# Fire led lighting

`esp32c3` and `neopixel` with 26 leds connected to gpio7 to create a beautiful fire lighting.

![example.jpg](/example.jpg)

## Known issues

For some reason the first led is always green.