//! Blinks an LED
//!
//! This assumes that a LED is connected to GPIO4.
//! Depending on your target and the board you are using you should change the pin.
//! If your board doesn't have on-board LEDs don't forget to add an appropriate resistor.
//!

use colorsys::{Hsl, Rgb};
use esp_idf_hal::peripherals::Peripherals;
use esp_idf_hal::spi::config::{Config, DriverConfig};
use esp_idf_hal::spi::SpiDriver;
use esp_idf_hal::units::{FromValueType, Hertz};
use esp_idf_hal::{delay::FreeRtos, spi::SpiDeviceDriver};
use rand::rngs::SmallRng;
use rand::{Rng, SeedableRng};
use ws2818_rgb_led_spi_driver::adapter_gen::{HardwareDev, WS28xxAdapter, WS28xxGenAdapter};
use ws2818_rgb_led_spi_driver::encoding;

struct Esp32Spi<'s>(SpiDeviceDriver<'s, SpiDriver<'s>>);

impl<'s> HardwareDev for Esp32Spi<'s> {
    fn write_all(&mut self, encoded_data: &[u8]) -> Result<(), String> {
        self.0.write(encoded_data).map_err(|e| format!("{e:?}"))
    }
}

fn main() -> anyhow::Result<()> {
    esp_idf_sys::link_patches();

    let peripherals = Peripherals::take().unwrap();
    let mosi = peripherals.pins.gpio7;
    let miso = peripherals.pins.gpio8;
    let cs = peripherals.pins.gpio9;
    let sclk = peripherals.pins.gpio10;

    let baudrate: Hertz = 15600u32.kHz().into();

    let spi = SpiDeviceDriver::new_single(
        peripherals.spi2,
        sclk,
        mosi,
        Some(miso),
        Some(cs),
        &DriverConfig::default(),
        &Config::default().baudrate(baudrate),
    )
    .unwrap_or_else(|e| {
        println!("Failed to initiate SPI: {e:?}");
        loop {}
    });

    let mut adapter = WS28xxGenAdapter::new(Box::new(Esp32Spi(spi)));

    let diodes = 30;

    adapter.clear(diodes);

    //     peripherals.SPI2,
    //     sclk,
    //     mosi,
    //     miso,
    //     cs,
    //     100u32.kHz(),
    //     SpiMode::Mode0,
    //     &mut peripheral_clock_control,
    //     &mut clocks,
    // );
    println!("Starting loop");

    let diodes = 26;

    let mut colors = vec![(0, 0, 0); diodes];
    let mut time = 1.0;
    let cold: Hsl = Rgb::from((2.0, 0.0, 0.0)).into();
    let hot: Hsl = Rgb::from((100.0, 80.0, 0.0)).into();

    let mut temperatures: Vec<f64> = vec![0.0; diodes];
    let mut temp2 = temperatures.clone();
    let mut rng = SmallRng::from_seed([1; 16]);

    let side_propagation = 0.3;
    let decay = 0.98;
    let kernel = [
        side_propagation,
        (1.0 - 2.0 * side_propagation),
        side_propagation,
    ];
    let spark_rareness = 10;
    let spark_brightness = 0.5;

    loop {
        time += 0.01;

        if rng.gen_range(0..spark_rareness) == 0 {
            let disturbed = rng.gen_range(0..diodes);
            temperatures[disturbed] = temperatures[disturbed] + spark_brightness;
        }

        for (i, window) in temperatures.windows(3).enumerate() {
            temp2[i + 1] =
                (window[0] * kernel[0] + window[1] * kernel[1] + window[2] * kernel[2]) * decay;
        }
        temp2[0] *= kernel[1] * decay;
        temp2[diodes - 1] *= kernel[1] * decay;

        std::mem::swap(&mut temperatures, &mut temp2);

        colors.iter_mut().enumerate().for_each(|(i, c)| {
            let color: Rgb = lerp(&cold, &hot, temperatures[i].clamp(0.0, 1.0)).into();
            c.0 = color.red() as u8;
            c.1 = color.green() as u8;
            c.2 = color.blue() as u8;
        });

        let encoded = encoding::encode_rgb_slice(&colors);

        adapter.write_encoded_rgb(&encoded).unwrap();
        FreeRtos::delay_ms(1);
        if time > 10000.0 {
            time = 0.0;
        }
    }
}

fn lerp(c1: &Hsl, c2: &Hsl, p: f64) -> Hsl {
    let mut result = Hsl::default();
    let dh = c2.hue() - c1.hue();
    let dl = c2.lightness() - c1.lightness();
    let ds = c2.saturation() - c1.saturation();
    result.set_hue(c1.hue() + dh * p);
    result.set_lightness(c1.lightness() + dl * p);
    result.set_saturation(c1.saturation() + ds * p);
    result
}
